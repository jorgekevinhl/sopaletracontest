package Negocio;


/**
 * Write a description of interface OperacionMatriz here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public interface OperacionMatriz
{
    //Retorna la cantidad de columnas en una fila
    public int getCantCol(int fila);
    
    //retorna el indice fila con la mayor cantidad de datos
    public int filaMasDatos();
    
}
