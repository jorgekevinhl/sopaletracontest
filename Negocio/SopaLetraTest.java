package Negocio;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class SopaLetraTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class SopaLetraTest
{
    /**
     * Default constructor for test class SopaLetraTest
     */
    public SopaLetraTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp() //metodos para base de datos
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test 
    public void testCrearMatriz() throws Exception
    {
        String prueba= "ana,ma,p";
        SopaLetra s= new SopaLetra(prueba);
        char rta[][]=s.getSopa();
        char ideal_matriz[][]={{'a','n','a'}, {'m','a'},{'p'}};
        //si rta == ideal_matriz --> C:        
        boolean paso = esIgual(rta, ideal_matriz);
        assertEquals(true,paso);
        
    }
    
    @Test 
    public void testCrearMatriz2() throws Exception
    {
        String prueba= "ana,maaa";
        SopaLetra s= new SopaLetra(prueba);
        char rta[][]=s.getSopa();
        char ideal_matriz[][]={{'a','n','a'}, {'m','a','a','a'}};
        //si rta == ideal_matriz --> C:        
        boolean paso = esIgual(rta, ideal_matriz);
        assertEquals(true,paso);
        
    }
    
    @Test 
    public void testCrearMatrizMal() throws Exception
    {
        String prueba= "ana,ma,p";
        SopaLetra s= new SopaLetra();
        char rta[][]=s.getSopa();
        char ideal_matriz[][]={{'a','n','a'}, {'m','a'},{'p'}};
        //si rta == ideal_matriz --> C:
        boolean paso = esIgual(rta, ideal_matriz);
        assertEquals(false,paso);
        
    }

    
    private boolean esIgual(char m1[][], char m2[][])
    {
    
        if(m1==null || m2==null || m1.length != m2.length)
            return false;
    
            
        for(int i1=0, i2=0; i1<m1.length && i2<m2.length; i1++, i2++)
        {
            if(!sonIgualesVectores(m1[i1], m2[i2]))
                return false;
        }
        
        return true;
    }
    
    private boolean sonIgualesVectores(char v1[], char v2[])
    {
        if(v1.length != v2.length)
            return false;
        
        for(int j=0;j<v1.length; j++)
        {
            if(v1[j]!=v2[j])
                return false;
        }
        
        return true;
    }
    
    @Test
    public void testEsCuadrada() throws Exception
    {
        String prueba= "ana,mao,pal";
        SopaLetra s= new SopaLetra(prueba);
        char rta [][] = s.getSopa();
        boolean paso = s.esCuadrada();
        assertEquals(true,paso);
    }
    
    @Test
    public void testEsCuadrada2() throws Exception
    {
        String prueba= "Juan,Mati,Pepe,Jose";
        SopaLetra s= new SopaLetra(prueba);
        char rta [][] = s.getSopa();
        boolean paso = s.esCuadrada();
        assertEquals(true,paso);
    }
    
    @Test
    public void testEsCuadrada3() throws Exception
    {
        String prueba= "an,pa";
        SopaLetra s= new SopaLetra(prueba);
        char rta [][] = s.getSopa();
        boolean paso = s.esCuadrada();
        assertEquals(true,paso);
    }
    
    
    @Test
    public void testEsCuadradaMal() throws Exception
    {
        String prueba= "ana,mao,pa";
        SopaLetra s= new SopaLetra(prueba);
        char rta [][] = s.getSopa();
        boolean paso = s.esCuadrada();
        assertEquals(false,paso);
    }
    
    @Test
    public void testEsCuadradaMal2() throws Exception
    {
        String prueba= "lopez,ramirez,tigre,jose,ignacio";
        SopaLetra s= new SopaLetra(prueba);
        char rta [][] = s.getSopa();
        boolean paso = s.esCuadrada();
        assertEquals(false,paso);
    }
    
    @Test
    public void testEsDispersa() throws Exception
    {
        String prueba= "anara,mao,palo";
        SopaLetra s= new SopaLetra(prueba);
        char rta [][] = s.getSopa();
        boolean paso = s.esDispersa();
        assertEquals(true,paso);
    }
    
    @Test
    public void testEsDispersa2() throws Exception
    {
        String prueba= "joshe,sajfnsa,asdn";
        SopaLetra s= new SopaLetra(prueba);
        char rta [][] = s.getSopa();
        boolean paso = s.esDispersa();
        assertEquals(true,paso);
    }
    
    @Test
    public void testEsDispersa3() throws Exception
    {
        String prueba= "jony,boris,rafael";
        SopaLetra s= new SopaLetra(prueba);
        char rta [][] = s.getSopa();
        boolean paso = s.esDispersa();
        assertEquals(true,paso);
    }
    
    @Test
    public void testEsDispersaMal() throws Exception
    {
        String prueba= "pepe,pepe,pepe,pepe";
        SopaLetra s= new SopaLetra(prueba);
        char rta [][] = s.getSopa();
        boolean paso = s.esDispersa();
        assertEquals(false,paso);
    }
    
    @Test
    public void testEsDispersaMal2() throws Exception
    {
        String prueba= "p";
        SopaLetra s= new SopaLetra(prueba);
        char rta [][] = s.getSopa();
        boolean paso = s.esDispersa();
        assertEquals(false,paso);
    }
    
    @Test
    public void testDiagonal() throws Exception
    {
        String prueba= "ana,mao,pal";
        SopaLetra s= new SopaLetra(prueba);
        char rta [] = s.getDiagonarInferior();
        char diagonal[]= {'a','a','l'};
        boolean paso= sonIgualesVectores(rta,diagonal);
        assertEquals(true,paso);
    }
    
    @Test
    public void testDiagonal2() throws Exception
    {
        String prueba= "mama,mama,mama,mama";
        SopaLetra s= new SopaLetra(prueba);
        char rta [] = s.getDiagonarInferior();
        char diagonal[]= {'m','a','m','a'};
        boolean paso= sonIgualesVectores(rta,diagonal);
        assertEquals(true,paso);
    }
    
    @Test
    public void testDiagonal3() throws Exception
    {
        String prueba= "tat,tat,tat";
        SopaLetra s= new SopaLetra(prueba);
        char rta [] = s.getDiagonarInferior();
        char diagonal[]= {'t','a','t'};
        boolean paso= sonIgualesVectores(rta,diagonal);
        assertEquals(true,paso);
    }
    
    @Test
    public void testDiagonalMal() throws Exception
    {
        String prueba= "loc,loc,loc";
        SopaLetra s= new SopaLetra(prueba);
        char rta [] = s.getDiagonarInferior();
        char diagonal[]= {'a','a','l'};
        boolean paso= sonIgualesVectores(rta,diagonal);
        assertEquals(false,paso);
    }
    
    @Test
    public void testDiagonalMal2() throws Exception
    {
        String prueba= "ana,mao,paro";
        SopaLetra s= new SopaLetra(prueba);
        char rta [] = s.getDiagonarInferior();
        char diagonal[]= {'a','a','l'};
        boolean paso= sonIgualesVectores(rta,diagonal);
        assertEquals(false,paso);
    }
    
    @Test
    public void testMasRepite() throws Exception
    {
        String prueba= "ana,mama,avianca";
        SopaLetra s= new SopaLetra(prueba);
        char letraRepite = 'a';
        char rta = s.getMas_SeRepite();
        assertEquals(letraRepite,rta);
    }
    
    @Test
    public void testMasRepite2() throws Exception
    {
        String prueba= "lolo,coco,poco,compro";
        SopaLetra s= new SopaLetra(prueba);
        char letraRepite = 'o';
        char rta = s.getMas_SeRepite();
        assertEquals(letraRepite,rta);
    }
    
    @Test
    public void testMasRepite3() throws Exception
    {
        String prueba= "papa,tata,kaka,flaca,hamaca";
        SopaLetra s= new SopaLetra(prueba);
        char letraRepite = 'a';
        char rta = s.getMas_SeRepite();
        assertEquals(letraRepite,rta);
    }
    
    @Test
    public void testMasRepiteMal() throws Exception
    {
        String prueba= "ana,mama,avianca";
        SopaLetra s= new SopaLetra(prueba);
        char letraRepite = 'v';
        char rta = s.getMas_SeRepite();
        assertEquals(letraRepite,rta);
    }
    
    @Test
    public void testMasRepiteMal2() throws Exception
    {
        String prueba= "bombo,tombo,rompo,corro";
        SopaLetra s= new SopaLetra(prueba);
        char letraRepite = 't';
        char rta = s.getMas_SeRepite();
        assertEquals(letraRepite,rta);
    }
    
   
} 
