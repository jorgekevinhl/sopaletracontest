package Negocio;

/**
 * Write a description of class SopaLetra here.
 * 
 * @author (Jorge Kevin Hernández Lasso) 
 * @version (22/08/2019)
 */
public class SopaLetra implements OperacionMatriz
{
    // La matriz con las letras de la sopa
    private char sopa[][];

    /**
     * Constructor for objects of class SopaLetras
     */
    public SopaLetra()
    {

    }

    public SopaLetra(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("No se puede crear la sopa");
        }

        String palabras2[]=palabras.split(",");
        //Crear la cantidad de filas:
        this.sopa=new char[palabras2.length][];
        //recorrer cada elemento de palabras2 y pasarlo a su correspondiente fila en sopa:
        int i=0;
        for(String palabraX: palabras2)
        {
            this.sopa[i]=new char[palabraX.length()];
            pasar(palabraX, this.sopa[i]);
            i++;
        }

    }

    private void pasar(String x, char vector[])
    {
        for(int j=0;j<x.length();j++)
            vector[j]=x.charAt(j);
    }

    public String toString()
    {
        String msg="";
        for(int i=0;i<this.sopa.length;i++)
        {
            for (int j=0;j<this.sopa[i].length;j++)
            {
                msg+=this.sopa[i][j]+""+"\t";
            }
            msg+="\n";
        }
        return msg;
    }

    public String toString2()
    {
        String msg="";
        for(char vector[]:this.sopa)
        {
            for (char dato:vector)
            {
                msg+=dato+""+"\t";
            }
            msg+="\n";
        }
        return msg;
    }

    /**
    Retorna la letra que más se repite
     */
    public char getMas_SeRepite() throws Exception
       {
        char ind= '\0';
        char ind2= '\0';
        int cont= 0;
        int cont2= 0;
        
        for(int k=0; k<sopa.length; k++)
        {
            for(int l=0; l<sopa[k].length; l++)
            {
            ind= sopa[k][l];
            for(int i=0; i<sopa.length; i++)
            {
               for(int j=0; j<sopa[i].length; j++)
               {
                   if(ind == sopa[i][j])
                    cont++;
               }
            }
        }
            if(cont2<cont)
            {
                ind2= ind;
                cont2= cont;
            }
            cont= 0;
        } 
    
        return ind2;
       }

    public boolean esCuadrada()
    {
        
        for(int i=0; i<this.sopa.length; i++ )
        {
            for(int j=0; j<this.sopa[i].length;j++){
                if(sopa.length!=sopa[i].length)
                {
                    return false;
                }
            }   
        } 
        return true;
    }

    public boolean esRectangular()
    {
        int col= sopa[0].length;
        for(int i=0; i<sopa.length; i++)
        {
            for(int j=0; j<sopa[i].length; j++)
            {
                if(col== sopa.length || col!= sopa[i].length)
                    return false;
            }
        }
        return true;
    }

    public boolean esDispersa()
    {
        int col= sopa[0].length;
        if(sopa.length>1)
        {
            for(int i=0; i<this.sopa.length; i++ )
            {
                for(int j=0; j<this.sopa[i].length;j++){
                    if( this.sopa[i].length != col)
                    {
                        return true; 
                    }
                }   
            }
        } 
        return false;
    }

    public char []getDiagonarInferior() throws Exception
    {
        //si y solo si es cuadrada , si no, lanza excepcion
        char  [] Diagonal = new char [this.sopa.length];
        if(esCuadrada())
        {
            for(int i=0; i<sopa.length; i++)
            {
                for(int j=0; j<sopa[i].length; j++)
                {
                    if( i == j)
                    {
                        Diagonal[i] = sopa[i][j];
                    }
                }
            }
        } else {
            throw new Exception("No se puede encontrar la diagonal, matriz no cuadrada");
        }
        return Diagonal;
    }

    
    //Start GetterSetterExtension Code
    /**Getter method sopa*/
    public char[][] getSopa(){
        return this.sopa;
    }//end method getSopa

    //End GetterSetterExtension Code
    //!
    
    public int getCantCol(int fila)
    {
        if(sopa==null || sopa.length<=fila || fila<0)
            return -1;
           
        return sopa[fila].length;   
    }
    
    
    //DEBE REPLANTEAR ESTE METODO
    public int filaMasDatos()
    {
     int cont1=0;
     int cont2=0;
     int puntero=0;
        for(int i=0; i<sopa.length; i++)
        {
            cont1= getCantCol(i);
            if(cont2<cont1)
            {
                cont2=cont1;
                puntero= i;
            }
        }
        return puntero;
    }
}
